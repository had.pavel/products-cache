# Products cache

## Installation

V hlavní složce projektu nastartujeme docker

```bash
docker compose up
```

Jdeme do www složky projektu a stáhneme potřebné závislosti

```bash
cd ./www
composer install
```

vložíme do databáze potřebné záznamy ze souboru pohovor.sql, například přes [adminer](http://localhost:8080/)

## Usage

Otevřeme si okno prohlížeče a [kocháme se tou nádherou](http://localhost:8081/)

## License
I don't mind