<?php

declare(strict_types=1);

namespace App\CurrysModule\Factory;

use App\CurrysModule\Models\ProductModel;
use App\CurrysModule\Models\ModelInterface;

final class ProductFactory extends BaseFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return ModelInterface|null
     */
    public static function create(array $data): ?ModelInterface
    {
        $product = new ProductModel();

        $product->setId($data['id']);
        $product->setName($data['name']);
        $product->setPrice($data['price']);

        return $product;
    }

    /**
     * @inheritDoc
     */
    public static function createFromDatabaseData(array $data): ?ModelInterface
    {
        return self::create($data);
    }

    public static function createFromJsonString(string $data): ?ModelInterface
    {
        $dataArray = json_decode($data, true);
        return self::create($dataArray);
    }

}