<?php

declare(strict_types=1);

namespace App\CurrysModule\Factory;

use App\CurrysModule\Models\ModelInterface;

interface FactoryInterface
{

    /**
     * @param array $data
     * @return ModelInterface|null
     */
    public static function createFromDatabaseData(array $data): ?ModelInterface;

}