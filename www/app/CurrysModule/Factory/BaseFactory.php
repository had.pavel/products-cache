<?php

declare(strict_types=1);

namespace App\CurrysModule\Factory;

use Nette\SmartObject;

abstract class BaseFactory
{
    use SmartObject;
}