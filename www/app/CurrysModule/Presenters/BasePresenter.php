<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    protected function beforeRender()
    {
        $this->template->addFilter('highlight', function (string $sentence, ?string $find = null, ?bool $caseSensitive = true) {
            if (empty($find)) {
                return $sentence;
            }

            if ($caseSensitive) {
                $pos = strpos($sentence, $find);
            } else {
                $pos = strpos(strtolower($sentence), strtolower($find));
            }

            if ($pos !== false) {
                $sentence = substr_replace($sentence, '</b>', $pos + strlen($find), 0);
                $sentence = substr_replace($sentence, '<b>', $pos, 0);
            }

            return $sentence;
        });
    }
}