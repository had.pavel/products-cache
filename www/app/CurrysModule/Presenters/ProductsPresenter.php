<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CurrysModule\Managers\ProductManager;
use App\CurrysModule\Models\ChannelGroupModel;
use App\CurrysModule\Repository\ChannelGroupRepository;

final class ProductsPresenter extends BasePresenter
{

    /**
     * @var ProductManager
     */
    private ProductManager $productManager;

    /**
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @return void
     */
    public function actionDefault()
    {
        $products = [$this->productManager->get(2)];
        $this->template->products = $products;
    }

}