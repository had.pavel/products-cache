<?php

declare(strict_types=1);

namespace App\CurrysModule\Services;

use Nette\Database\Table\ActiveRow;

class ProductCacheService implements CacheServiceInterface
{

    public const FILENAME = '../temp/cache/products.json';

    /**
     * @inheritDoc
     */
    public function get(int $id): ?string
    {
        if (!file_exists($this::FILENAME)) {
            return null;
        }

        $content = file_get_contents($this::FILENAME);
        $products = json_decode($content, true);

        if (is_null($products)) {
            return null;
        }

        foreach ($products as $product) {
            if ($product['id'] === $id) {
                return json_encode($product);
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function insert(ActiveRow $data): void
    {
        if (!is_null($this->get($data['id']))) {
            return;
        }

        if (file_exists($this::FILENAME)) {
            $content = file_get_contents($this::FILENAME);
            $products = json_decode($content);
            $products[] = json_decode(json_encode($data->toArray()));

            $cache = fopen($this::FILENAME, "w");
            fwrite($cache, json_encode($products));
        } else {
            $cache = fopen($this::FILENAME, "w");
            fwrite($cache, json_encode([$data->toArray()]));
        }

        fclose($cache);
    }

}