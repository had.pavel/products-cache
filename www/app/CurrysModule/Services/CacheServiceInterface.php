<?php

namespace App\CurrysModule\Services;

use Nette\Database\Table\ActiveRow;

interface CacheServiceInterface
{

    /**
     * @param int $id
     * @return array|null
     */
    public function get(int $id): ?string;

    /**
     * @param ActiveRow $data
     * @return void
     */
    public function insert(ActiveRow $data): void;

}