<?php

declare(strict_types=1);

namespace App\CurrysModule\Managers;

use App\CurrysModule\Models\ProductModel;
use App\CurrysModule\Repository\ProductRepository;

final class ProductManager
{
    /**
     * @var ProductRepository
     */
    private ProductRepository $productRepository;

    private string $searchEngine;

    /**
     * @param string $searchEngine
     * @param ProductRepository $productRepository
     */
    public function __construct(
        string            $searchEngine,
        ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->searchEngine = $searchEngine;
    }

    public function get(int $id): ?ProductModel
    {
        switch ($this->searchEngine) {
            case 'mysql':
                return $this->productRepository->findProduct($id);
                break;
            case 'elastic':
                return $this->productRepository->findById($id);
                break;
        }

        return null;
    }
}