<?php

declare(strict_types=1);

namespace App\CurrysModule\Repository;

use Nette\SmartObject;
use Nette\Database\Explorer;

abstract class BaseRepository implements IElasticSearchDriver, IMySQLDriver
{
    use SmartObject;

    /**
     * @var Explorer
     */
    protected Explorer $database;

    /**
     * @param Explorer $database
     */
    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }

}