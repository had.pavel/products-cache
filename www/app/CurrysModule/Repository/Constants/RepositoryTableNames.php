<?php

declare(strict_types=1);

namespace App\CurrysModule\Repository\Constants;

class RepositoryTableNames
{
    const PRODUCT_TABLE_NAME = 'Products';
}