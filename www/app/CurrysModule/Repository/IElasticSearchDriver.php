<?php

declare(strict_types=1);

namespace App\CurrysModule\Repository;

interface IElasticSearchDriver
{
    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id);
}