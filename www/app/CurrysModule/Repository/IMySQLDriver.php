<?php

declare(strict_types=1);

namespace App\CurrysModule\Repository;

interface IMySQLDriver
{
    /**
     * @param int $id
     * @return mixed
     */
    public function findProduct(int $id);
}