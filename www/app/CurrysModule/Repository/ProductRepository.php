<?php

declare(strict_types=1);

namespace App\CurrysModule\Repository;

use App\CurrysModule\Factory\ProductFactory;
use App\CurrysModule\Models\ProductModel;
use App\CurrysModule\Repository\Constants\RepositoryTableNames;
use Nette\Database\Explorer;
use App\CurrysModule\Services\ProductCacheService;


final class ProductRepository extends BaseRepository
{

    /**
     * @var ProductCacheService
     */
    private ProductCacheService $productCacheService;

    /**
     * @param Explorer $database
     * @param ProductCacheService $productCacheService
     */
    public function __construct(
        Explorer            $database,
        ProductCacheService $productCacheService
    )
    {
        parent::__construct($database);
        $this->productCacheService = $productCacheService;
    }

    /**
     * @param ProductModel $productModel
     * @return void
     */
    public function insert(ProductModel $productModel): void
    {
        $tableName = RepositoryTableNames::PRODUCT_TABLE_NAME;

        $result = $this->database
            ->table($tableName)
            ->insert([
                'name' => $productModel->getName(),
                'price' => $productModel->getPrice()
            ]);
    }

    /**
     * Elastic
     * @inheritDoc
     */
    public function findById($id): ?ProductModel
    {
        return null;
    }

    /**
     * MySQL
     * @inheritDoc
     */
    public function findProduct(int $id): ?ProductModel
    {
        $productData = $this->productCacheService->get($id);

        if (!is_null($productData)) {
            return ProductFactory::createFromJsonString($productData);
        } else {
            $tableName = RepositoryTableNames::PRODUCT_TABLE_NAME;

            $result = $this->database
                ->table($tableName)
                ->select("
                    $tableName.id, 
                    $tableName.name, 
                    $tableName.price")
                ->where("$tableName.id", $id)
                ->fetch();

            $this->productCacheService->insert($result);

            return ProductFactory::createFromDatabaseData($result->toArray());
        }
    }

}